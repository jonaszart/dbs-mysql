const connect = require('./src/connect');
const getRepositories = require('./src/repositories');

module.exports = {
  connect,
  getRepositories
};
