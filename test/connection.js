const assert = require('assert');
const { randomName, randomNumber } = require('jrz-helpers');
const {
  connect,
  getRepositories
} = require('../index');

describe('', async () => {
  it('should connect', async () => {
    const connection = await connect('mysql://root:example@localhost:3306/dbsdb');
    assert.equal(connection.isConnected, true);
  });
  it('should insert and get shopping cart', async () => {
    const ShoppingCart = await getRepositories('ShoppingCart');
    assert.equal(typeof ShoppingCart, 'object');
    const toInsert = {
      userId: randomNumber(111, 999)
    };
    const insert = await ShoppingCart.save(toInsert);
    const cart = await ShoppingCart.findOne({ id: insert.id });
    assert.equal(cart.id, insert.id);
  });
  it('should insert and get product cart', async () => {
    const ProductsCart = await getRepositories('ProductsCart');
    assert.equal(typeof ProductsCart, 'object');
    const toInsert = {
      shoppingCardId: randomNumber(11, 999),
      productId: randomNumber(11, 999),
      price: randomNumber(11, 9999),
      quantity: randomNumber(1, 9)
    };
    const insert = await ProductsCart.save(toInsert);
    const pCart = await ProductsCart.findOne({ id: insert.id });
    assert.equal(pCart.id, insert.id);
  });
});
