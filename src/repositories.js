const database = require('typeorm');

module.exports = async entity => database
	.getConnection('default')
	.getRepository(entity);
