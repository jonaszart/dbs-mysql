const { EntitySchema } = require('typeorm');

module.exports = new EntitySchema({
  name: 'ShoppingCart',
  columns: {
    id: {
      primary: true,
      type: 'int',
      generated: true
    },
    userId: {
      type: 'varchar'
    }
  },
  indices: [{
    name: 'userId_i',
    unique: false,
    columns: ['userId']
  }]
});
