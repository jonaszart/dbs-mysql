const { EntitySchema } = require('typeorm');

module.exports = new EntitySchema({
  name: 'ProductsCart',
  columns: {
    id: {
      primary: true,
      type: 'int',
      generated: true
    },
    shoppingCartId: {
      type: 'int',
      generated: false
    },
    productId: {
      type: 'varchar'
    },
    price: {
      type: 'float'
    },
    quantity: {
      type: 'int',
      generated: false
    }
  },
  indices: [{
    name: 'shoppingCartId_i',
    unique: false,
    columns: ['shoppingCartId']
  }, {
    name: 'productId_i',
    unique: false,
    columns: ['productId']
  }]
});
