const ProductsCart = require('./ProductsCart');
const ShoppingCart = require('./ShoppingCart');

module.exports = [
	ProductsCart,
	ShoppingCart
];
