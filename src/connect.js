const database = require('typeorm');
const { makeLogger } = require('jrz-logger');
const entities = require('./entities');

const logger = makeLogger(process.pid, process.env.LOG_LEVEL);

module.exports = async (connectionString) => {
  if (!connectionString) return database;
  const connection = await database
    .createConnection({
    	type: 'mysql',
      synchronize: true,
    	url: connectionString,
      entities
    })
    .catch((e) => { logger.error(`Database connection failed ${e}`); });
  if (!connection || !connection.isConnected) {
    logger.warn('MYSQL NOT Connected');
  } else {
    logger.info('MYSQL Connected');
  }
  return connection || database;
};
